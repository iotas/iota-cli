package toolkit

import (
	"fmt"
	"github.com/guonaihong/gout"
	"time"
)

const TimeOut = time.Second * 5

var defaultHeader = gout.H{
	"Accept":     "application/json",
	"User-Agent": "IOTA/beta",
}

type RepoCommits []*RepoCommit

type RepoCommit struct {
	URL         string `json:"url"`
	Sha         string `json:"sha"`
	HTMLURL     string `json:"html_url"`
	CommentsURL string `json:"comments_url"`
	Commit      struct {
		Author struct {
			Name  string `json:"name"`
			Date  string `json:"date"`
			Email string `json:"email"`
		} `json:"author"`
		Committer struct {
			Name  string `json:"name"`
			Date  string `json:"date"`
			Email string `json:"email"`
		} `json:"committer"`
		Message string `json:"message"`
		Tree    struct {
			Sha string `json:"sha"`
			URL string `json:"url"`
		} `json:"tree"`
	} `json:"commit"`
	Author struct {
		ID                int    `json:"id"`
		Login             string `json:"login"`
		Name              string `json:"name"`
		AvatarURL         string `json:"avatar_url"`
		URL               string `json:"url"`
		HTMLURL           string `json:"html_url"`
		Remark            string `json:"remark"`
		FollowersURL      string `json:"followers_url"`
		FollowingURL      string `json:"following_url"`
		GistsURL          string `json:"gists_url"`
		StarredURL        string `json:"starred_url"`
		SubscriptionsURL  string `json:"subscriptions_url"`
		OrganizationsURL  string `json:"organizations_url"`
		ReposURL          string `json:"repos_url"`
		EventsURL         string `json:"events_url"`
		ReceivedEventsURL string `json:"received_events_url"`
		Type              string `json:"type"`
	} `json:"author"`
	Committer struct {
		ID                int    `json:"id"`
		Login             string `json:"login"`
		Name              string `json:"name"`
		AvatarURL         string `json:"avatar_url"`
		URL               string `json:"url"`
		HTMLURL           string `json:"html_url"`
		Remark            string `json:"remark"`
		FollowersURL      string `json:"followers_url"`
		FollowingURL      string `json:"following_url"`
		GistsURL          string `json:"gists_url"`
		StarredURL        string `json:"starred_url"`
		SubscriptionsURL  string `json:"subscriptions_url"`
		OrganizationsURL  string `json:"organizations_url"`
		ReposURL          string `json:"repos_url"`
		EventsURL         string `json:"events_url"`
		ReceivedEventsURL string `json:"received_events_url"`
		Type              string `json:"type"`
	} `json:"committer"`
	Parents []struct {
		Sha string `json:"sha"`
		URL string `json:"url"`
	} `json:"parents"`
}

// GetRepoLatestCommit 获取仓库最新提交记录
func GetRepoLatestCommit() (*RepoCommit, error) {
	var commits RepoCommits

	api := fmt.Sprintf("https://gitee.com/api/v5/repos/iotas/iota-cli/commits")

	if err := gout.GET(api).SetHeader(defaultHeader).SetQuery(gout.H{
		"page":     1,
		"per_page": 5,
	}).SetTimeout(TimeOut).BindJSON(&commits).Do(); err != nil {
		return nil, err
	}

	if len(commits) == 0 {
		return nil, fmt.Errorf("empty commit")
	}
	return commits[0], nil
}
