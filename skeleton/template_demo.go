package skeleton

const templateController = `package controller

import (
	"github.com/gin-gonic/gin"
	"{{.Name}}/common"
)

func PostDemo(c *gin.Context)  {
	var payload struct {
		Ping string
	}
	err := common.Read(c.Request.Body, &payload)
	if err != nil {
		_, _ = c.Writer.WriteString(common.FailResponse(common.CodeParamError,err.Error()))
		return
	}

	var resp = struct {
		Pong string
	}{Pong: payload.Ping}
	_, _ = c.Writer.WriteString(common.SuccessResponse(resp))
}

func GetDemo(c *gin.Context)  {
	var resp = struct {
		Ping string
	}{Ping: "Pong"}
	_, _ = c.Writer.WriteString(common.SuccessResponse(resp))
}

`
const templateServices = `package services

import "context"

type Test struct {
	Appid     string 
}

func (test *Test) Test(c context.Context)  error {
	return nil
}
`
const templateLogic = `package logic

import "context"

type Test struct {
	Appid     string 
}

func (test *Test) Test(c context.Context)  error {
	return nil
}
`
