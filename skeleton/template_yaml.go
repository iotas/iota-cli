package skeleton

const templateLocalYaml = `server-name: {{.Name}}
alarm-channel-id: 39
server-listen: 0.0.0.0:12580
grpc-server-listen: 0.0.0.0:12581
environment: local
redis-pool:
  network: tcp
  address: 10.0.0.135:6379
  max-idle: 10
  max-active: 200
  wait: true
  password: admin
mongo-conf:
  uri: mongodb://mdbc:mdbc@10.0.0.135:27117/admin
  conn-timeout: 10
  pool-size: 4
  db-name: mdbc
jaeger-conf:
  jaeger-service-name: {{.Name}}
  jaeger-sampler-cfg:
    jaeger-sampler-type: const
    jaeger-sampler-param: 1
  jaeger-agent-host: 10.0.0.135:6831
  jaeger-log-spans: true
  jaeger-disabled: false
`

const templateDevYaml = `server-name: {{.Name}}
alarm-channel-id: 39
server-listen: 0.0.0.0:12580
grpc-server-listen: 0.0.0.0:12581
environment: dev
redis-pool:
  network: tcp
  address: 0.0.0.0:6379
  max-idle: 10
  max-active: 200
  wait: true
  password: admin
trace-agent-host: close
trace-disabled: true
mongo-conf:
  uri: mongodb://admin:admin@10.0.0.135:27017/admin
  conn-timeout: 10
  pool-size: 4
  db-name: admin
jaeger-conf:
  jaeger-service-name: {{.Name}}
  jaeger-sampler-cfg:
    jaeger-sampler-type: const
    jaeger-sampler-param: 1
  jaeger-agent-host: 10.0.0.135:6831
  jaeger-log-spans: true
  jaeger-disabled: false
`

const templateProdYaml = `server-name: {{.Name}}
alarm-channel-id: 39
server-listen: 0.0.0.0:12580
grpc-server-listen: 0.0.0.0:12581
environment: prod
redis-pool:
  network: tcp
  address: 0.0.0.0:6379
  max-idle: 10
  max-active: 200
  wait: true
  password: admin
trace-agent-host: close
trace-disabled: true
mongo-conf:
  uri: mongodb://admin:admin@10.0.0.135:27017/admin
  conn-timeout: 10
  pool-size: 4
  db-name: admin
jaeger-conf:
  jaeger-service-name: {{.Name}}
  jaeger-sampler-cfg:
    jaeger-sampler-type: const
    jaeger-sampler-param: 1
  jaeger-agent-host: 10.0.0.135:6831
  jaeger-log-spans: true
  jaeger-disabled: false
`
