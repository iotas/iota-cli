package skeleton

const templateConfig = `package config

import (
	"github.com/spf13/cobra"
	"gitlab.heywoods.cn/go-sdk/omega/driver"
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"os"
)

var Cfg *ServerConf

type ServerConf struct {
	ConfigFile      string
	driver.Config                  	 ` + "`" + `yaml:",inline"` + "`" + `
	ServerListen    string       	 ` + "`" + `yaml:"server-listen"` + "`" + `
	AlarmChannelId  string		 	 ` + "`" + `yaml:"alarm-channel-id"` + "`" + `
	Environment  	string		 	 ` + "`" + `yaml:"environment"` + "`" + `
	Mysql       	*mysqlConf   	 ` + "`" + `yaml:"mysql"` + "`" + `
}

type mysqlConf struct {
	Url          string ` + "`" + `yaml:"url"` + "`" + `
	MaxIdleConns int    ` + "`" + `yaml:"max-idle-conns"` + "`" + `
	ConnMaxLife  int    ` + "`" + `yaml:"conn-max-life"` + "`" + `
}

func NewDefaultConfig(cmd *cobra.Command) *ServerConf {
	c := &ServerConf{}
	return c
}

func (c *ServerConf) LoadConfigFile() error {
	f, err := os.Open(c.ConfigFile)
	if err != nil {
		return err
	}
	defer f.Close()
	data, err := ioutil.ReadAll(f)
	if err != nil {
		return err
	}
	if err := yaml.Unmarshal(data, c); err != nil {
		return err
	}
	return nil
}
`
