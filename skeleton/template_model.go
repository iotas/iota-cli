package skeleton

const templateProto = `syntax = "proto3";

import "google/protobuf/timestamp.proto";
package model;


option go_package = "{{.Name}}/model";

// @route_group: true
// @route_api: /api/user
// @gen_to: ./internal/controller/user_controller.go
service UserAPI {
    // @desc: 登录接口
    // @method: post
    // @api: /login
    // @author: 匿名
    rpc Login    (LoginReq) returns (LoginResp);
}

message LoginReq {
    // @desc: 用户名
    string username = 1; // 在这里
    // @desc: 密码
    string password = 2;
}

message LoginResp {}


// @bson: true
// @table_name: tb_friends_info
message HelloWorld {
    string id 	 = 1; // 主键ID wxid md5
    string hello = 2; // 妳好
    string world = 3; // 世界
}
`
