package skeleton

const templateMain = `package main

import "{{.Name}}/cmd"

func main() {
	cmd.Execute()
}
`
