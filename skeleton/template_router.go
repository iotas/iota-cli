package skeleton

const templateRouter = `package router

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.heywoods.cn/go-sdk/omega/driver"
	"{{.Name}}/common"
	"{{.Name}}/config"
	"{{.Name}}/internal/controller"

	"gitlab.heywoods.cn/go-sdk/omega/component/log"
    "{{.Name}}/infra/middleware"
	"gitlab.heywoods.cn/go-sdk/omega/core"
)

type Router struct {
	*log.Logger
	Cfg *config.ServerConf
}

// InitRouter 注册路由
func (rou *Router) InitRouter() error {
	gin.SetMode(gin.ReleaseMode)
	router := gin.New()
	router.Use(gin.Recovery(), middleware.CorsFilter(), middleware.SetHeader())
	
	router.GET("/demo",controller.GetDemo)
	router.POST("/demo",controller.PostDemo)
	
	// 从这里开始实例化路由注册器
	_ = core.NewRegister()

	rou.Info(context.TODO(), "begin Server Listen")

	return router.Run(rou.Cfg.ServerListen)
}

// NewRouter 初始化各种连接
func NewRouter(cfg *config.ServerConf) (srv *Router, err error) {
	// 加载配置文件
	if err = cfg.LoadConfigFile(); err != nil {
		return
	}

	drivers := driver.NewDriver(&cfg.Config)
	drivers.StartLogger().StartMongoDB().StartRedis()

	// 初始化 es_logger
	common.Logger = log.GetLogger()

	srv = &Router{
		Cfg:    cfg,
		Logger: log.GetLogger(),
	}
	return
}
`

const templateMiddleware = `package middleware

import (
	"github.com/gin-gonic/gin"
)

// CorsFilter 跨域过滤器
func CorsFilter() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Headers", "Authorization,X-CSRF-Token,Token,session,X_Requested_With,DNT,X-CustomHeader,X-Requested-With,If-Modified-Since,Pragma")
		c.Header("Access-Control-Allow-Methods", "*")
		c.Header("Access-Control-Expose-Headers", "*")
		if c.Request.Method == "OPTIONS" {
			c.JSON(200, "Options Request!")
			return
		}
	}
}

// SetHeader 统一设置返回的头部信息
func SetHeader() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Content-Type", "application/json")
	}
}
`
