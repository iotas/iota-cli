package skeleton

const templateModule = `module {{.Name}}

go 1.16

`

const templateModuleUpdate = `#!/bin/bash

# 该脚本用来自动全量更新该项目中用到的module

go mod tidy

match_required=$(cat go.mod | grep -zoE "\((.*?)\)" | awk -F ' ' '{print $1}' | awk '{if($1>1){print $1}}')

for i in $match_required;do go get -u "$i";done
`
