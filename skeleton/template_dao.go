package skeleton

const templateDaoRedis = `package rdbc

import (
	"github.com/go-redis/redis/v8"
)

var RedisPool redis.UniversalClient
`

const templateDaoRedisCacheKey = `package rdbc

var CacheKeys []string

func init() {
	CacheKeys = []string{}
}
`
