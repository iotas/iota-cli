module gitee.com/iotas/iota-cli

go 1.16

require (
	gitee.com/iotas/toolkit v0.0.0-20211215063554-6a2c4ec13c63
	github.com/elliotchance/pie v1.39.0
	github.com/go-playground/validator/v10 v10.9.0 // indirect
	github.com/guonaihong/gout v0.2.10
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.3.0
	golang.org/x/crypto v0.0.0-20211209193657-4570a0811e8b // indirect
	golang.org/x/net v0.0.0-20211215060638-4ddde0e984e9 // indirect
	golang.org/x/sys v0.0.0-20211214234402-4825e8c3871d // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
