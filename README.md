### builder

`builder`命令的详细介绍见 http://doc.heywoods.cn/web/#/47?page_id=371

### 安装

```shell
$ go install gitlab.heywoods.cn/go-sdk/builder@latest
## output
go: downloading gitlab.heywoods.cn/go-sdk/builder v0.0.0-20210902040340-7af67c79f914
```

安装完毕后执行 `builder -h` 验证是否安装成功

```shell
[builder@builder builder]$ builder -h
builder 是基于 omega 库的一个提高生产效率的工具链

Usage:
  builder [command]

Available Commands:
  addapi                 给路由组新增一个api
  addroute               快速添加一个路由组
  addrpc                 给service新增一个rpc
  auth                   进行gitlab授权认证, 否则部份授权操作无法进行
  create                 创建一个新项目
  dep                    更新builder依赖的工具链
  fmt                    格式化 proto 文件使其看的赏心悦目
  gen                    解析proto文件, 自动生成开发代码.
  gen-k8s-deployment-yml 生成k8s deployment yml文件
  gen-k8s-ingress-yml    生成k8s ingress yml文件
  gitter                 拉取git仓库代码并布署
  go                     golang 版本管理
  help                   Help about any command
  md                     给路由组的api输出markdown文档
  repo                   gitlab仓库操作
  run                    快速运行项目
  tool                   将文件构建为任意工具箱
  update                 检测并更新builder
  version                打印builder版本信息

Flags:
  -h, --help   help for builder

Use "builder [command] --help" for more information about a command.

```

### builder 版本更新

在安装完 `builder` 后, 可以使用内置的 `update` 命令进行自更新. 当然, builder 内置有检测是否有新版本的机制,当版本过低,将提醒你手动更新

```shell
[builder@builder builder]$ builder update
-----
更新 builder 插件中...

builder 更新完成
-----

```

### builder 依赖工具链更新

在安装完 `builder` 后, 务必使用内置的 `dep` 命令进行工具链更新.

```shell
[builder@builder builder]$ builder dep
检测 protoc 插件中...
protoc local version: v3.18.0
protoc latest version: v3.18.1
正在下载 protoc...
插件将被放置到: /home/x/go/bin/protoc
include文件夹将被放置在: /home/x/go/bin/include
protoc version: libprotoc 3.18.1
安装 protoc完毕
-----
更新 protoc-go-inject-tag 插件中...
protoc-go-inject-tag更新完成
-----
更新 protoc-gen-go 插件中...
protoc-gen-go更新完成
-----
更新 protoc-gen-go-grpc 插件中...

protoc-gen-go-grpc 更新完成
-----
-----
更新 builder 插件中...

builder 更新完成
-----
```

### 新建项目

在当前目录新建一个名为 `MyProject` 的项目

```shell
[builder@builder builder]$ builder create --name MyProject --path .
```
